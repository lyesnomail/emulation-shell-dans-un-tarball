


void lstar(char * tarr)//Fonction ls classique
{   
    int pos = whenbackn(tarr);char * tar = malloc(sizeof(tarr));
    memset(tar,'\0',sizeof(tarr));
    if (pos!=-1){strncpy(tar,tarr,pos);}else {tar=tarr;}
    int fd;
    char * bef = beftar(tar);
    char * aft = afttar(tar);
    char * nm;
    int z=0;
    if(tarmode==0){
        z=1;
        if(istar(tar)&&!dottar(tar)){
            fd = open(bef,O_RDONLY);nm=bef;
        }
        else{
            fd = open(tar,O_RDONLY);nm=tar;} }
    else{
        fd = open(tarname,O_RDONLY);nm=tarname;
        if(strcmp(path,"\0")!=0){
            char * pathcopy = malloc((strlen(path)*sizeof(char)+(strlen(tar)*sizeof(tar))));
            memset(pathcopy,'\0',strlen(pathcopy)*sizeof(char));
            strncpy(pathcopy,path,strlen(path));
            strcat(pathcopy,"/");
            strcat(pathcopy,tar);
            aft  = pathcopy;
        }
        else{ aft=tar;}
    }
    if(((!(isfile(aft,nm)||isdir(aft,nm)))&&(strlen(aft)!=0))||(fd<0))// Partie qui récurrente dans le programme, et qui traite les cas d'erreurs tels que l'inexistence d'une fiche/doss
    {
        char * msg = malloc(TAILLE*sizeof(char));memset(msg,'\0',sizeof(msg));
        char * m1 = "ls: impossible d'accéder à '";
        char * m2 = "': Aucun fichier ou dossier de ce type";
        strcat(msg,m1);
        strcat(msg,tar);
        strcat(msg,m2);
        write(STDIN_FILENO,msg,strlen(msg));
    }
    else{
        int n,i;
        struct posix_header *h = (struct posix_header*)malloc(sizeof(struct posix_header));
        while (1)
        {
            n = read(fd,h,sizeof(struct posix_header ));
            if((strcmp(h->name, "\0")==0)){
                break;
            }
            if((strcmp(aft,"\0" )!=0)){
                if((strcmp(h->name,aft)==0)&&(h->typeflag=='0')){
                    int pos = back(h->name);
                    char * a = malloc((strlen(h->name)-pos)*sizeof(char));
                    strncpy(a,h->name+pos,(strlen(h->name)-pos));
                    write(STDIN_FILENO,a,strlen(a));
                }
                if((strncmp(h->name,aft,strlen(aft))==0)){
                    if((strlen(h->name)-z)!=(strlen(aft))){
                    if(outside(h->name,aft)==1){
                    strcat(h->name," ");
                    write(STDIN_FILENO,strcat(hname(h->name,aft),"\n"),strlen(hname(h->name,aft)));
                }
                }}
            }
            else{
                strcat(h->name," ");
                i = 0;
                if(outside(h->name,aft)==1){
                    write(STDIN_FILENO,hname(h->name,aft),strlen(h->name));}
            }
            sscanf(h->size,"%o",&i);
            i = (i + BLOCKSIZE - 1) >> BLOCKBITS ;
            lseek(fd,i*BLOCKSIZE,SEEK_CUR);
        }free(h);}
    write(STDIN_FILENO,"\n",strlen("\n"));
    close(fd);
}


char * rev(char * buf)//Fonction qui renverse l'ordre des caractère 'une chaine
{
    char * rev = malloc(sizeof(buf));
    memset(rev, '\0', sizeof(buf));
    int i,k,j;
    for(i = 0; i < strlen(buf); i++)
    {
        rev[i] = buf[strlen(buf)-i-1];
    }
    return rev;
}
char * convperm(char perm)//Fonction qui permet de convertir en chaine rwx les droit en octale d'un fich/doss
{
    char * res = malloc(sizeof(char)*3);
    memset(res, '\0', sizeof(char)*3);
    int n = atoi(&perm);
    int b,i;
    for(i=0; i<3; i++)
    {
        b = n%2;
        n = n/2;
        if ( i == 0 ) {if ( b == 1 ){strcat(res,"x");} else {strcat(res, "-");} }
        if ( i == 1 ) { if (b == 1){  strcat(res, "w"); } else { strcat(res, "-");} }
        if ( i == 2 ) { if (b == 1){  strcat(res, "r"); } else { strcat(res, "-");} }
    }
    char * revc = rev(res);
    return revc;
}
char * perm(char * perm)//Fonction qui permet la construction des chaine des droits
{
    char * permfinal = malloc(sizeof(char)*9);
    memset( permfinal, '\0', sizeof(char)*9 );
    int i=strlen(perm)-3;
    while(i!=strlen(perm)){
        strcat(permfinal, convperm(perm[i]));
        i++;
    }
    return permfinal;
}
char * timetar(char * h)// Fonction qui génère une chaine contenant la date de création d'un fichier/doss dans un tar
{
    time_t t;
    long long c =atoll(h);
    t = (time_t) c;
    char *t1 = ctime(&t);
    if (t1[strlen(t1)-1] == '\n') {t1[strlen(t1)-1] = '\0';}
    return t1;
}
char * sizetar(char * size,struct posix_header *h)//Fonction qui génère une chaine contenant la taille d'un fich/doss dans un tar
{
    int n = atoi(size);
    if (h->typeflag==53){n = 4096;} // normalement 5, c'est etrange !!
    char * s = malloc(sizeof(int));
    memset(s,'\0',sizeof(s));
    sprintf(s, "%d", n);
    return s;
}
void afftar(struct posix_header *h,char* aft)//Fonction d'affichage pour ls -l
{
    if (h->typeflag=='5'){
        write(STDIN_FILENO,"d",strlen("d"));
    }else{
        write(STDIN_FILENO,"-",strlen("-"));
    }
    char * a = hname(h->name,aft);
    char * aft1 = malloc(sizeof(aft)+sizeof(char));
    memset(aft1,'\0',sizeof(aft));
    strncpy(aft1,aft,strlen(aft));
    strcat(aft1,"/ ");
    if (strcmp(h->name,aft1)!=0){
        write(STDIN_FILENO,perm(h->mode),strlen(perm(h->mode)));
        write(STDIN_FILENO," ",strlen(" "));
        write(STDIN_FILENO,h->uname,strlen(h->uname));
        write(STDIN_FILENO," ",strlen(" "));
        write(STDIN_FILENO,h->gname,strlen(h->gname));
        write(STDIN_FILENO," ",strlen(" "));
        write(STDIN_FILENO,sizetar(h->size,h),strlen(sizetar(h->size,h)));
        write(STDIN_FILENO," ",strlen(" "));
        write(STDIN_FILENO,timetar(h->mtime),strlen(timetar(h->mtime)));
        write(STDIN_FILENO," ",strlen(" "));
        write(STDIN_FILENO,a,strlen(a));
        write(STDIN_FILENO," ",strlen(" "));
        write(STDIN_FILENO,"\n",strlen("\n"));}
}
void afftar2(struct posix_header *h,char* aft)
{
    int pos = back(h->name);
    char * a = malloc((strlen(h->name)-pos)*sizeof(char));
    strncpy(a,h->name+pos,(strlen(h->name)-pos));
    write(STDIN_FILENO,"-",strlen("-"));
    write(STDIN_FILENO,perm(h->mode),strlen(perm(h->mode)));
    write(STDIN_FILENO," ",strlen(" "));
    write(STDIN_FILENO,h->uname,strlen(h->uname));
    write(STDIN_FILENO," ",strlen(" "));
    write(STDIN_FILENO,h->gname,strlen(h->gname));
    write(STDIN_FILENO," ",strlen(" "));
    write(STDIN_FILENO,sizetar(h->size,h),strlen(sizetar(h->size,h)));
    write(STDIN_FILENO," ",strlen(" "));
    write(STDIN_FILENO,timetar(h->mtime),strlen(timetar(h->mtime)));
    write(STDIN_FILENO," ",strlen(" "));
    write(STDIN_FILENO,a,strlen(a));
    write(STDIN_FILENO," ",strlen(" "));
    write(STDIN_FILENO,"\n",strlen("\n"));
}
void lsminltar(char * tarr)//Fonction ls -l
{
    int pos = whenbackn(tarr);char * tar = malloc(sizeof(tarr));
    memset(tar,'\0',sizeof(tarr));
    if (pos!=-1){strncpy(tar,tarr,pos);}else {tar=tarr;}
    int fd;
    char * bef = beftar(tar);
    char * aft = afttar(tar);
    char * nm;
    int z=0;
    if(tarmode==0){
        z=1;
        if(istar(tar)&&!dottar(tar)){
            fd = open(bef,O_RDONLY);nm=bef;
        }
        else{
            fd = open(tar,O_RDONLY);nm=tar;} }
    else{
        fd = open(tarname,O_RDONLY);nm=tarname;
        if(strcmp(path,"\0")!=0){
            char * pathcopy = malloc((strlen(path)*sizeof(char)+(strlen(tar)*sizeof(tar))));
            memset(pathcopy,'\0',strlen(pathcopy)*sizeof(char));
            strncpy(pathcopy,path,strlen(path));
            strcat(pathcopy,"/");
            strcat(pathcopy,tar);
            aft  = pathcopy;
        }
        else{ aft=tar;}
    }
    int n,i;
    struct posix_header *h = (struct posix_header*)malloc(sizeof(struct posix_header));
    if(((!(isfile(aft,nm)||isdir(aft,nm)))&&(strlen(aft)!=0))||(fd<0)){
        char * msg = malloc(TAILLE*sizeof(char));memset(msg,'\0',sizeof(msg));
        char * m1 = "ls: impossible d'accéder à '";
        char * m2 = "': Aucun fichier ou dossier de ce type";
        strcat(msg,m1);
        strcat(msg,tar);
        strcat(msg,m2);
        write(STDIN_FILENO,msg,strlen(msg));
    }
    else{
        while (1)
        {
            n = read(fd,h,sizeof(struct posix_header ));
            if((strcmp(h->name, "\0")==0)){
                break;
            }
            if((strcmp(aft,"\0" )!=0)){
                if((strcmp(h->name,aft)==0)&&(h->typeflag=='0')){
                    afftar2(h,aft);
                }
                if((strncmp(h->name,aft,strlen(aft))==0)){
                    if((strlen(h->name)-z)!=(strlen(aft))){
                        if(outside(h->name,aft)==1){
                            strcat(h->name," ");
                            afftar(h,aft);}
                }}
            }
            else{
                strcat(h->name," ");
                i = 0;
                if(outside(h->name,aft)==1){
                    afftar(h,aft);}
            }
            sscanf(h->size,"%o",&i);
            i = (i + BLOCKSIZE - 1) >> BLOCKBITS ;
            lseek(fd,i*BLOCKSIZE,SEEK_CUR);
        }
        free(h);}
    close(fd);
}
void cat(char *ff)//Fonction cat
{
    int pos = whenbackn(ff);char * f = malloc(sizeof(ff));
    memset(f,'\0',sizeof(f));
    if (pos!=-1){strncpy(f,ff,pos);}else {f=ff;}
    int fd;
    char * bef = beftar(f);
    char * aft = malloc(sizeof(afttar(f)));
    memset(aft,'\0',strlen(afttar(f)));
    strncpy(aft,afttar(f),strlen(afttar(f)));
    char * nm;
    if(tarmode==0){
        if(istar(f)&&!dottar(f)){
            fd = open(bef,O_RDONLY);nm = bef;
        }
        else{
            fd = open(f,O_RDONLY);nm=f;} }
    else{
        fd = open(tarname,O_RDONLY);nm = tarname;
        if(strcmp(path,"\0")!=0){
            char * pathcopy = malloc((strlen(path)*sizeof(char)+(strlen(f)*sizeof(f))));
            memset(pathcopy,'\0',strlen(pathcopy)*sizeof(char));
            strncpy(pathcopy,path,strlen(path));
            strcat(pathcopy,"/");
            strcat(pathcopy,f);
            aft = pathcopy;
        }
        else{
            aft = f;
        }
    }
    if(((!(isfile(aft,nm)||isdir(aft,nm)))&&(strlen(aft)!=0))||(fd<0)){
        char * msg = malloc(TAILLE*sizeof(char));memset(msg,'\0',sizeof(msg));
        char * m1 = "cat: ";
        char * m2 = ": Aucun fichier ou dossier de ce type";
        strcat(msg,m1);
        strcat(msg,f);
        strcat(msg,m2);
        write(STDIN_FILENO,msg,strlen(msg));
    }
    else{
        struct posix_header * h = (struct posix_header*) malloc(sizeof(struct posix_header));
        while (1)
        {
            int n =read(fd,h,sizeof(struct posix_header));
            if(n==0 || h->name[0]=='\0'){
                break;
            }
            if (!strcmp(h->name,aft))
            {
                break;
            }
            int i = 0;
            sscanf(h->size,"%o",&i);
            i = (i + BLOCKSIZE - 1) >> BLOCKBITS ;
            lseek(fd,i*BLOCKSIZE,SEEK_CUR);
        }
        int i = 0;
        sscanf(h->size,"%o",&i);
        if (i==0)
        {
            close(fd);
        }
        else
        {
            char *data = malloc(i);
            read(fd , data , i);
            write(STDIN_FILENO,data,i);
            write(STDIN_FILENO,"\n",1);
            free(data);
            close(fd);
        }
        }
}
