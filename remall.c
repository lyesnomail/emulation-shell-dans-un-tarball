int nbrelem(char * tar, char typeflag)//Fonction qui renvoie le nombre déliement ayant un certain type flag dans un tar
{
  int fd;int nbr=0;
  fd = open(tar,O_RDONLY);
  int n,i;
  struct posix_header *h = (struct posix_header*)malloc(sizeof(struct posix_header));
  while (1)
  {
      n = read(fd,h,sizeof(struct posix_header ));
      if((strcmp(h->name, "\0")==0)){
          break;
      }
      if(h->typeflag==typeflag){
        strcat(h->name," ");
        nbr++;
      }
      else{
        strcat(h->name," ");
      }
      i = 0;
      sscanf(h->size,"%o",&i);
      i = (i + BLOCKSIZE - 1) >> BLOCKBITS ;
      lseek(fd,i*BLOCKSIZE,SEEK_CUR);
  }

  close(fd);
  return nbr;
}

char ** listfiles(char * tar)//Fonction qui renvoie à partir d'un tar la liste de tous les fichiers qui s'y trouvent
{
  int fd; char ** t = malloc((nbrelem(tar,'0'))*sizeof(char));
  fd = open(tar,O_RDONLY);
  int n,i,j=0;
  struct posix_header *h = (struct posix_header*)malloc(sizeof(struct posix_header));
  while (1)
  {
      n = read(fd,h,sizeof(struct posix_header ));
      if((strcmp(h->name, "\0")==0)){
          break;
      }
      if(h->typeflag=='0'){
        t[j]=malloc(strlen(h->name)*sizeof(char));
        memset(t[j],'\0',strlen(t[j]));
        strncpy(t[j],h->name,strlen(h->name)*sizeof(char));
        j = j+1;
      }
      else{
        strcat(h->name," ");
      }
      i = 0;
      sscanf(h->size,"%o",&i);
      i = (i + BLOCKSIZE - 1) >> BLOCKBITS ;
      lseek(fd,i*BLOCKSIZE,SEEK_CUR);
  }

  close(fd);
  return t;
}

char ** listdir(char * tar)//Fonction qui renvoie à partir d'un tar la liste de tous les dossiers qui s'y trouvent
{
  int fd;char ** t = malloc((nbrelem(tar,'5'))*sizeof(char));
  fd = open(tar,O_RDONLY);
  int n,i,j=0;
  struct posix_header *h = (struct posix_header*)malloc(sizeof(struct posix_header));
  while (1)
  {
      n = read(fd,h,sizeof(struct posix_header ));
      if((strcmp(h->name, "\0")==0)){
          break;
      }
      if(h->typeflag=='5'){
        t[j]=malloc(strlen(h->name)*sizeof(char));
        memset(t[j],'\0',strlen(t[j]));
        strncpy(t[j],h->name,strlen(h->name)*sizeof(char));
        j = j+1;
      }
      i = 0;
      sscanf(h->size,"%o",&i);
      i = (i + BLOCKSIZE - 1) >> BLOCKBITS ;
      lseek(fd,i*BLOCKSIZE,SEEK_CUR);
  }

  close(fd);
  return t;
}

void rem(char * tar,char * pa)//Fonction qui permet de supprimer un fichier dans un tar ( conçu pour rm -r)
{
  int fd = open(tar,O_RDONLY);
  int b = position_file_before(pa,beftar(tar));
  int a = position_file_after(pa,beftar(tar));
  int fd1 = open ("temp",O_CREAT|O_WRONLY,0777);
  char c;
  for (size_t i = 0; i < b; i++)
  {
      read(fd,&c,1);
      write(fd1,&c,1);
  }
  lseek(fd,a,SEEK_SET);
  while (read(fd,&c,1)!=0)
  {
      write(fd1,&c,1);
  }
  remove(beftar(tar));
  rename("temp",beftar(tar));
}

int position(char *pa,char* del)//Fonction qui renvoie la position d'un fichier dans un tar ( conçu pour rm -r)
{
   int fd = open(del,O_RDONLY);
   struct posix_header *h = malloc(sizeof(struct posix_header));
   while (1)
   {
       int n = read(fd,h,sizeof(struct posix_header));
       if(h->name[0]=='\0' || n!= BLOCKSIZE || n==0){
           break;
       }
       if(strcmp(h->name,pa)==0){
       if (h->typeflag=='5'){

           int p =lseek(fd,0,SEEK_CUR);
           p=p-512;
           close(fd);
           free(pa);
           return p;
       }
   }
       int i = 0;
       sscanf(h->size,"%o",&i);
       i = (i + BLOCKSIZE - 1) >> BLOCKBITS ;
       lseek(fd,i*BLOCKSIZE,SEEK_CUR);
   }

   close(fd);
   free(pa);
   return 0;
}

void remdir(char * tar,char * pa)//FFonction qui permet de supprimer un dossier dans un tar ( conçu pour Rm -r)
{
    int fd = open(tar,O_RDONLY);
    int b = position(pa,tar);
    int new_file=open("temp.tar",O_WRONLY | O_CREAT,0777);
    char c ;
    int n ;
    while ((n = lseek(fd,0,SEEK_CUR) )< b)
    {
        read(fd,&c,1);
        write(new_file,&c,1);
    }
    lseek(fd,512,SEEK_CUR);
    while (read(fd,&c,1)!=0)
    {
        write(new_file,&c,1);
    }
    close(fd);
    close(new_file);
    remove(tar);
    rename("temp.tar",tar);
}

void rmminr(char * buff)//Fonction rm -r, ( supprime tous les fichiers, puis tous les dossiers devenus vides )
{
    int pos = whenbackn(buff);char * buf = malloc(sizeof(buff));
    memset(buf,'\0',sizeof(buf));
    if (pos!=-1){strncpy(buf,buff,pos);}else {buf=buff;}
    int fd;
    char * bef = beftar(buf);
    char * aft = afttar(buf);
    char ** t1;
    char ** t2;
    char * tarnm;
    int nbrfich,nbrdoss;
    int z=0;
    if(tarmode==0){
      z=1;
      if(istar(buf)&&!dottar(buf)){
        t1 = listfiles(bef);t2 = listdir(bef);
        nbrfich = nbrelem(bef,'0');nbrdoss = nbrelem(bef,'5');
        tarnm=bef;
      }
      else{
        perror("erreur nom du fichier");
      } }
    else{
      t1 = listfiles(tarname);t2 = listdir(tarname); tarnm=tarname;
      nbrfich = nbrelem(tarname,'0');nbrdoss = nbrelem(tarname,'5');
      if(strcmp(path,"\0")!=0){
        char * pathcopy = malloc((strlen(path)*sizeof(char)+(strlen(buf)*sizeof(char))));
        memset(pathcopy,'\0',strlen(pathcopy)*sizeof(char));
        strncpy(pathcopy,path,strlen(path));
        strcat(pathcopy,"/");
        strcat(pathcopy,buf);
        aft  = pathcopy;
        }
        else{ aft=buf;}}
    if(!(isfile(aft,tarnm)||isdir(aft,tarnm))){
      char * msg = malloc(TAILLE*sizeof(char));memset(msg,'\0',sizeof(msg));
      char * m1 = "rm: ";
      char * m2 = ": Aucun fichier ou dossier de ce type";
      strcat(msg,m1);
      strcat(msg,aft);
      strcat(msg,m2);
      write(STDIN_FILENO,msg,strlen(msg));
    }
    else{
         for(int ii = 0; ii < nbrfich;ii++ ){
            if((strncmp(t1[ii],aft,strlen(aft))==0)){// on c jamé c peu etr strlen -1
              rem(tarnm,t1[ii]);
            }
          }
          for(int ii = 0; ii < nbrdoss;ii++ ){
            if((strncmp(t2[ii],aft,strlen(aft))==0)){
              remdir(tarnm,t2[ii]);
            }
          }
      }
}
