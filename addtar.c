
int size_file(char *name)//Fonction qui donne le nombre de block dans un fichier tar
{
    int fd = open(name , O_RDONLY);
    int p = lseek(fd,0,SEEK_END);
    close(fd);
    return p;
}
int sourcetype(char *p){
    int fd = open(p,O_RDONLY);
    if (fd<0)
    {
        close(fd);
        return -1;
    }
   struct stat s ;
   stat(p,&s);
   if (S_ISDIR(s.st_mode))
   {
       return 0; // dir
   }
   return 1 ; // file

}/// Fonction qui verifie si c'est un fichier, doss ou non, -1 il y a pas de fichier 
struct posix_header * tete_file(char *name)//Fonction de creation d'un bloque en tête pour dossier
{
    struct posix_header *h = malloc(sizeof(struct posix_header));
    strcpy(h->name,name);
    sprintf(h->mode,"0000700");
    h->mode[7]='\0';
    sprintf(h->size,"%011o",size_file(name));
    time_t begin = time( NULL );
    sprintf(h->mtime,"%lo",begin);
    h->typeflag='5';
    strcpy(h->magic,"ustar");
    strcpy(h->version,"00");
    set_checksum(h);
    return h ;
}
struct posix_header * tete_file2(char *name){
    struct stat s;
    struct posix_header *h = malloc(sizeof(struct posix_header));
    stat(name,&s);
    strcpy(h->name,name);
    sprintf(h->mode,"0000700");
    h->mode[7]='\0';
    sprintf(h->size,"%lo",s.st_size);
    sprintf(h->mtime,"%lo",s.st_mtime);
    h->typeflag='0';
    strcpy(h->magic,"ustar");
    strcpy(h->version,"00");
    set_checksum(h);
    return h ;
}
struct posix_header *getDir(char * name)
{
    struct stat s;
    struct posix_header *h = malloc(sizeof(struct posix_header));
    stat(name,&s);
    char *p = malloc(strlen(name)+1);
    strcat(p,name);
    strcat(p,"/");
    strcpy(h->name,p);
    free(p);
    sprintf(h->mode,"0000700");
    h->mode[7]='\5';
    sprintf(h->size,"%o",0);
    sprintf(h->mtime,"%lo",s.st_mtime);
    h->typeflag='5';
    strcpy(h->magic,"ustar");
    strcpy(h->version,"00");
    set_checksum(h);
    return h ;
}
struct posix_header * tete_file_file(char *name)//Fonction de creation d'un bloque en tête pour fichier
{
     struct stat s;
     struct posix_header *h = malloc(sizeof(struct posix_header));
     stat(name,&s);
     strcpy(h->name,name);
     sprintf(h->mode,"0000700");
     h->mode[7]='\0';
     sprintf(h->size,"%lo",s.st_size);
     sprintf(h->mtime,"%lo",s.st_mtime);
     h->typeflag='0';
     strcpy(h->magic,"ustar");
     strcpy(h->version,"00");
     set_checksum(h);
     return h ;
}
void add_dir_in_tar(char *tarr)//Fonction Mkdir
{
    int pos = whenbackn(tarr);char * tar = malloc(sizeof(tarr));
    memset(tar,'\0',sizeof(tarr));
    if (pos!=-1){strncpy(tar,tarr,pos);}else {tar=tarr;}
    int fd;
    char * bef = beftar(tar);
    char * aft = afttar(tar);
    char * del;
    int z=0;
    if(tarmode==0){
        z=1;
        if(istar(tar)&&!dottar(tar)){
            fd = open(bef,O_RDONLY);del = bef;
        }
        else{
            fd = open(tar,O_RDONLY);del = tar;} }
    else{
      fd = open(tarname,O_RDONLY); del = tarname;
      if(strcmp(path,"\0")!=0){
          char * pathcopy = malloc((strlen(path)*sizeof(char)+(strlen(tar)*sizeof(tar))));
          memset(pathcopy,'\0',strlen(pathcopy)*sizeof(char));
          strncpy(pathcopy,path,strlen(path));
          strcat(pathcopy,"/");
          strcat(pathcopy,tar);
          aft  = pathcopy;
          }
          else{ aft=tar;}
    }
    if(isdir(aft,beftar(del))){
        char * msg = malloc(TAILLE*sizeof(char));memset(msg,'\0',sizeof(msg));
        char * m1 = "mkdir: impossible de créer le répertoire «";
        char * m2 = "»: Le fichier existe";
        strcat(msg,m1);
        strcat(msg,tar);
        strcat(msg,m2);
        write(STDIN_FILENO,msg,strlen(msg));
    }
    else if((fd<0)&&(!tarmode)){
        open(beftar(del),O_CREAT,0777);
        add_dir_in_tar(tar);
    }
    else {
        int new_file=open("temp",O_WRONLY | O_CREAT,0777);
        char * namefile = malloc((strlen(aft)*sizeof(char)+sizeof(char)));
        memset(namefile,'\0',sizeof(namefile));
        strncpy(namefile,aft,strlen(aft));
        strcat(namefile,"/");
        struct posix_header *p = tete_file(namefile);
        write(new_file,p,BLOCKSIZE);
        free(p);
        char c;
        while (read(fd,&c,1)!=0)
        {
          write(new_file,&c,1);
        }
        
        remove(del);
        rename("temp",del);}
}
int position_fichier(char *p)//Fonction qui determine la position d'un fichier donné dans un tar
{
   int fd = open(tarname,O_RDONLY);
   char *pa = malloc(sizeof(char)*(strlen(p)+1));
   strcpy(pa,p);
   strcat(pa,"/");
   struct posix_header *h = malloc(sizeof(struct posix_header));
   while (1)
   {
       int n = read(fd,h,sizeof(struct posix_header));
       if(h->name[0]=='\0' || n!= BLOCKSIZE || n==0){
           break;
       }
       if(strcmp(h->name,pa)==0){
       if (h->typeflag=='5'){
           int p = lseek(fd,0,SEEK_CUR);
           close(fd);
           free(h);
           return p;
       }
   }
       int i = 0;
       sscanf(h->size,"%o",&i);
       i = (i + BLOCKSIZE - 1) >> BLOCKBITS ;
       lseek(fd,i*BLOCKSIZE,SEEK_CUR);
   }
   free(h);
   close(fd);
   free(pa);
   return 0;
}
void extraire(char  *pa ,char  *dossier )
{
   int fd = open(tarname,O_RDONLY);
   struct posix_header *h = malloc(sizeof(struct posix_header));
   while (1)
   {
       int n = read(fd,h,sizeof(struct posix_header));
       if(h->name[0]=='\0' || n!= BLOCKSIZE || n==0){
           break;
       }
       if(strcmp(h->name,pa)==0){
           char * name = malloc(100);
           char * name2 = malloc(100);
           strcat(name,dossier);
           strcat(name,"/");int zz=0;
           int posb=back(pa);if(strlen(path)!=0){zz=1;}
           strncpy(name2,pa+posb+zz,strlen(pa)-posb);
           strcat(name,name2);
           strcpy(h->name,name);
           free(name);
           set_checksum(h);
           int i = 0;
           sscanf(h->size,"%o",&i);
           if (i%BLOCKSIZE!=0)
           {
               int rest = BLOCKSIZE - (i%BLOCKSIZE) ;
               i = i+ rest ;
           }
           char c ;
           int new_file= open("info_fichier",O_CREAT|O_WRONLY,0777);
           write(new_file,h,BLOCKSIZE);
           for (size_t j = 0; j < i; j++)
           {
               read(fd,&c,1);
               write(new_file,&c,1);
           }
           close(new_file);
           break;
   }
       int i = 0;
       sscanf(h->size,"%o",&i);
       i = (i + BLOCKSIZE - 1) >> BLOCKBITS ;
       lseek(fd,i*BLOCKSIZE,SEEK_CUR);
   }
   free(h);
   close(fd);
}
int tar_exisiter(char *path)
{
    if(strlen(path) <= 4 || strcmp(&path[strlen(path)-4],".tar")!=0){
        return -2;
    }
     int fd=open(path,O_RDONLY);
     if (fd<0) {
         close(fd);
         perror("il y a pas de fichier \n");
         return -1;}
         close(fd);
         return 0;
 }
int exisiter(char *path)// Fonction qui verifie l'existance d'un fichier
{
     int fd=open(path,O_RDONLY);
     if (fd<0) {
         close(fd);
         perror("il y a pas de fichier \n");
         return -1;}
         struct stat s;
         fstat(fd,&s);
         if(S_ISDIR(s.st_mode)){close(fd); return -2;}
         close(fd);
         return 0;
 }
void cree_temp_tar(char *name)
{
    int fd = open("temp.tar",O_CREAT|O_WRONLY,0777);
    struct posix_header *h =tete_file_file(name);
    write(fd,h,BLOCKSIZE);
    int taille = size_file(name);
    free(h);
    int rest ;
    if(taille%BLOCKSIZE==0){
        rest = 0 ;
    }
    else
    {
        rest = BLOCKSIZE - (taille%BLOCKSIZE);
    }
    int fd1 = open(name,O_RDONLY);
    char c;
    for (size_t i = 0; i < taille; i++)
    {
        read(fd1,&c,1);
        write(fd,&c,1);
    }
    if (rest !=0)
    {
        for (size_t i = 0; i < rest; i++)
        {
            write(fd,'\0',1);
        }
    }
    for (size_t i = 0; i < 2*BLOCKSIZE; i++)
    {
        write(fd,'\0',1);
    }
    close(fd1);
    close(fd);
}
void filetotar(char *s1 , char *s2)
{   
    char * path_to_tar = beftar(s2);
    char * path_aft_tar = afttar(s2);
    char * p;
    char * pathcopy = malloc((strlen(path)*sizeof(char)+(strlen(path_aft_tar)*sizeof(char))+sizeof(char)));
    memset(pathcopy,'\0',strlen(pathcopy)*sizeof(char));
    strncpy(pathcopy,path_aft_tar,strlen(path_aft_tar));
    if(strlen(path_aft_tar)!=0){
      strcat(pathcopy,"/");}
    strcat(pathcopy,s1);
    p  = pathcopy;
    int q = size_file(s2);
	struct posix_header *h1 = tete_file_file(p);
	int fd = open(beftar(s2),O_WRONLY);
	lseek(fd,-1024,SEEK_END);
	write(fd,h1,512);
	free(h1);
	char c ;
	int in = open(s1,O_RDONLY);
	while(read(in,&c,1)!=0){write(fd,&c,1);}
	close(in);
	if (q%512!=0)
                    {
                         for (size_t i = 0; i < (BLOCKSIZE - (q%512)); i++){
                        write(fd,"\0",1);
                        }
                        
                    }
                    for (size_t i = 0; i < 1024; i++){
    write(fd,"\0",1);
    }
    close(fd);


    
}
void cp_in_tar(char *souce , char * destionation)//Fonction qui copie un fichier depuis et vers le même fichier tar
{
   if (isfile(souce,tarname)==0)
   {
       perror("le fichier n'els st pas disponible !!");
   }
   else if (isdir(destionation,tarname) == 0)
   {
       perror("le dossier n'est pas disponible !!");
   }
   else
   {
       int postion = position_fichier(destionation);
       extraire(souce,destionation);
       int fd = open(tarname,O_RDONLY);
       int fd1 = open("temp",O_CREAT|O_WRONLY,0777);
       char c;
       for (size_t i = 0; i < postion; i++)
       {
           read(fd,&c,1);
           write(fd1,&c,1);
       }
       int fd2 = open("info_fichier",O_RDONLY);
       while (read(fd2,&c,1)!=0)
       {
           write(fd1,&c,1);
       }
       close(fd2);
       remove("info_fichier");
       while (read(fd,&c,1)!=0)
       {
           write(fd1,&c,1);
       }
       close(fd);
       close(fd1);
       remove(tarname);
       rename("temp",tarname);
   }
}
int position_dir(char const *p){
    int fd = open(tarname,O_RDONLY);
    char *pa = malloc(sizeof(char)*(strlen(p)+1));
    strcpy(pa,p);
    strcat(pa,"/");
    struct posix_header *h = malloc(sizeof(struct posix_header));
    while (1)
    {
        int n = read(fd,h,sizeof(struct posix_header));
        if(h->name[0]=='\0' || n!= BLOCKSIZE || n==0){
            break;
        }
        if(strcmp(h->name,pa)==0){
        if (h->typeflag=='5'){
            
            int s = lseek(fd,0,SEEK_CUR);
            close(fd);
            
            return s;
        }
    }
        int i = 0;
        sscanf(h->size,"%o",&i);
        i = (i + BLOCKSIZE - 1) >> BLOCKBITS ;
        lseek(fd,i*BLOCKSIZE,SEEK_CUR);
    }
    
    close(fd);
    
    return 0;
}
void cp_in_tar_dir(char *souce , char *destinaton){
    int fd = open(tarname,O_RDONLY);
    int ex = open("temp",O_CREAT|O_WRONLY,0777);
    struct posix_header *h = malloc(sizeof(struct posix_header));
    char *p3 = malloc(strlen(path)+1+strlen(destinaton));
    memset(p3,'\0',sizeof(p3));
    if(strlen(path)!=0){strcat(p3,path);strcat(p3,"/");}
    strcat(p3,destinaton);
     while (1)
     {
        int n = read(fd,h,sizeof(struct posix_header));
        if(h->name[0]=='\0' || n!= BLOCKSIZE || n==0){
            break;
        }
        char *p1 = malloc(strlen(path)+1+strlen(souce));
        memset(p1,'\0',sizeof(p1));
        if(strlen(path)!=0){strcat(p1,path);strcat(p1,"/");}
        strcat(p1,souce);
        if(strncmp(h->name,p1,strlen(p1))==0){
            char *p2 = malloc(TAILLE);   
            strcat(p2,p3);
            strcat(p2,"/");
            strcat(p2,h->name+strlen(p1)-strlen(souce));                               
            strcpy(h->name,p2);
            set_checksum(h);
            write(ex,h,BLOCKSIZE);
            free(p1);
            free(p2);
            
            int i = 0;
            sscanf(h->size,"%o",&i);
            if (i==0)
            {
                continue;
            }
            else{
                char c ;
                for (size_t k = 0; k < i; k++)
                {
                    read(fd,&c,1);
                    write(ex,&c,1);
                }
                if (i%512!=0)
                {
                    for (size_t bbb = 0; bbb < BLOCKSIZE-(i%512); bbb++)
                    {
                        write(ex,'\0',1);
                    }
                }
                
                continue;
            }
            }
        int i = 0;
        sscanf(h->size,"%o",&i);
        i = (i + BLOCKSIZE - 1) >> BLOCKBITS ;
        lseek(fd,i*BLOCKSIZE,SEEK_CUR);
    }
    close(ex);close(fd);
    int pos = position_dir(p3);
    int c1 = open(tarname,O_RDONLY);
    int fd5 = open("temp",O_RDONLY);
    int ntar = open("temp1",O_CREAT|O_WRONLY,0777);
    char c ;
    for (size_t i = 0; i < pos; i++)
    {
        read(c1,&c,1);
        write(ntar,&c,1);
    }
    while(read(fd5,&c,1)!=0){write(ntar,&c,1);}
    close(fd5);
    remove("temp");
    char *data = malloc(512);
    while(read(c1,data,1)!=0)
    {       
    if (data[0]=='\0')
    {
        break;
    }
    
    write(ntar,&c,1);}
    free(data);
    
    close(c1);
    remove(tarname);
    for (size_t ki = 0; ki < 1024; ki++)
    {
        write(ntar,"\0",1);
    }   
    close(ntar);
    rename("temp1",tarname);
}
void cp_in_tar_out(char * s1 , char * s2,char * tarnm )//Fonction qui copie un dossier exterieur vers le même fichier tar
{   
    char * souce = afttar(s1);
    char * destionation = afttar(s2);
    int postion = position_fichier(destionation);
    extraire(souce,destionation);
    int fd = open(tarnm,O_RDONLY);
    int fd1 = open("temp",O_CREAT|O_WRONLY,0777);
    char c;
    for (size_t i = 0; i < postion; i++)
    {
        read(fd,&c,1);
        write(fd1,&c,1);
    }
    int fd2 = open("info_fichier",O_RDONLY);
    while (read(fd2,&c,1)!=0)
    {
        write(fd1,&c,1);
    }
    close(fd2);
    remove("info_fichier");
    while (read(fd,&c,1)!=0)
    {
        write(fd1,&c,1);
    }
    close(fd);
    close(fd1);
    remove(tarnm);
    rename("temp",tarnm);

}
void dir_to_tar_rec (char *p ,int fd)
{    
    DIR *dirp = opendir(p);
    struct posix_header *h = getDir(p);
    write(fd,h,BLOCKSIZE);
    struct dirent *entry;
    free(h);
    struct stat s ; 
    while((entry=readdir(dirp))){
            char *nnn = malloc(strlen(p)+strlen(entry->d_name)+2);
            strcat(nnn,p);
            strcat(nnn,"/");
            strcat(nnn,entry->d_name);
            stat(nnn,&s);
            if (strcmp (entry->d_name,"..") == 0 || strcmp (entry->d_name,".") == 0)
            {
                free(nnn);
                continue;
            }
                if (S_ISDIR(s.st_mode))
                {
                    int pid = fork();
                    if(pid<0){
                    perror("eureur");
                    }
                    if(pid==0){
                    dir_to_tar_rec (nnn,fd);
                    free(nnn);
                    break;
                    }
                    int i;
                    wait(&i);
                }
                else
                {
                    struct posix_header *h1 =tete_file_file(nnn);
                    write(fd,h1,BLOCKSIZE);
                    free(h);
                    int r = size_file(nnn);
                    int fd_read = open(nnn,O_RDONLY);
                    char c;
                    while (read(fd_read,&c,1)!=0)
                    {
                        write(fd,&c,1);
                    }
                    if (r%512!=0)
                    {
                         for (size_t i = 0; i < (BLOCKSIZE - (r%512)); i++){
                        write(fd,"\0",1);
                        }
                        
                    }
                    
                    close(fd_read);
                    continue;
                }
            
    }
    closedir(dirp);
}
void run(char *p1,char *p2)//Fonction qui copie un dossier exterieur au tar vers celuis ci
{
    int fd = open(p2,O_RDWR);
    lseek(fd,-1024,SEEK_END);
     dir_to_tar_rec(p1,fd);
     for (size_t i = 0; i < 1024; i++){
    write(fd,"\0",1);
    }
    close(fd);
}
void mv(char * s1 , char * s2, int option)
{ 
  char * source;
  char * dest;
  if(tarmode==1){
    if(strcmp(path,"\0")!=0){
        char * pathcopy1 = malloc((strlen(path)*sizeof(char)+(strlen(s1)*sizeof(char))+sizeof(char)));
        memset(pathcopy1,'\0',strlen(pathcopy1)*sizeof(char));
        strncpy(pathcopy1,path,strlen(path));
        strcat(pathcopy1,"/");
        strcat(pathcopy1,s1);
        source=pathcopy1;
        char * pathcopy2 = malloc((strlen(path)*sizeof(char)+(strlen(s2)*sizeof(char))+sizeof(char)));
        memset(pathcopy2,'\0',strlen(pathcopy2)*sizeof(char));
        strncpy(pathcopy2,path,strlen(path));
        strcat(pathcopy2,"/");
        strcat(pathcopy2,s2);
        dest  = pathcopy2;
    }
    else{ source=s1;dest=s2;}
    if(isfile(source,tarname)){  
            cp_in_tar(source,dest);
            remove_file(s1);
            }
    
    else if(isdir(source,tarname))
    { if(isdir(dest,tarname)&&(option)){cp_in_tar_dir(source,dest); 
    
            int pidrm1=0;
            pidrm1 = fork();
            if(pidrm1 == 0){
                rmminr(s1);
                kill(getpid(),SIGTERM);
            }  
    }  }

  }
  else{
      if (istar(s1)){
          if(((istar(s1)&&(istar(s2)&&(strcmp(beftar(s2),beftar(s1))==0))))&&(option)){
              cp_in_tar_out(afttar(s1),afttar(s2),beftar(s2));             
              remove_dir_in_tar(s1);
          }
      }
      else{
        if(istar(s2)) {
            if(( sourcetype(s1) ==0 )&&(option)){                             
                run(s1,beftar(s2));char *
                 newdir = malloc(sizeof(s1)+sizeof(beftar(s2))+sizeof(char));
                 if(strlen(afttar(s2))!=0){tarname=beftar(s2);
                    mv(s1,afttar(s2),1);tarname="";}
                if(option = 1 ){
                    int pidmvext=0;
                    pidmvext = fork();
                    if(pidmvext == 0){
                        execl("/bin/rm","rm","-r",s1, (char *)NULL);
                    }
                }else{rmdir(s1);}
            }
            else if( sourcetype(s1) ==1){
                if ((isdir(afttar(s2),beftar(s2)))||(strlen(afttar(s2))==0))
                    {
                        filetotar(s1,s2);char * newdir = malloc(sizeof(s1)+sizeof(beftar(s2))+sizeof(char));
                        if(strlen(afttar(s2))!=0){tarname=beftar(s2);
                           mv(s1,afttar(s2),1);tarname="";}
                        }
                        int pidmvext=0;
                        pidmvext = fork();
                        if(pidmvext == 0){
                            execl("/bin/rm","rm","-r",s1, (char *)NULL);
                        }
                        }
            
        }
  }
  }
}
void cp(char * s1, char * s2, int option)
{            
  char * source;
  char * dest;
  if(tarmode==1){
    if(strcmp(path,"\0")!=0){
        char * pathcopy1 = malloc((strlen(path)*sizeof(char)+(strlen(s1)*sizeof(char))+sizeof(char)));
        memset(pathcopy1,'\0',strlen(pathcopy1)*sizeof(char));
        strncpy(pathcopy1,path,strlen(path));
        strcat(pathcopy1,"/");
        strcat(pathcopy1,s1);
        source=pathcopy1;
        char * pathcopy2 = malloc((strlen(path)*sizeof(char)+(strlen(s2)*sizeof(char))+sizeof(char)));
        memset(pathcopy2,'\0',strlen(pathcopy2)*sizeof(char));
        strncpy(pathcopy2,path,strlen(path));
        strcat(pathcopy2,"/");
        strcat(pathcopy2,s2);
        dest  = pathcopy2;
    }
    else{ source=s1;dest=s2;}
    if(isfile(source,tarname)){  
            cp_in_tar(source,dest);           
            }
    
    else if(isdir(source,tarname))
    {   if(isdir(dest,tarname)&&(option)){cp_in_tar_dir(s1,s2); } }
    
  }
  else{
      if (istar(s1)){
          if(((istar(s1)&&(istar(s2)&&(strcmp(beftar(s2),beftar(s1))==0))))&&(option)){
              cp_in_tar_out(afttar(s1),afttar(s2),beftar(s2));             
          }
      }
      else{
        if((istar(s2))) {
            if( (sourcetype(s1) ==0 ) &&(option)){                
                run(s1,beftar(s2));char * newdir = malloc(sizeof(s1)+sizeof(beftar(s2))+sizeof(char));
                 if(strlen(afttar(s2))!= 0){tarname=beftar(s2);
                            mv(s1,afttar(s2),1);tarname="";
                    }
            }
            else if( sourcetype(s1) ==1){
                if ((isdir(afttar(s2),beftar(s2)))||(strlen(afttar(s2))==0))
                    {
                        filetotar(s1,s2);
                        char * newdir = malloc(sizeof(s1)+sizeof(beftar(s2))+sizeof(char));
                        if(strlen(afttar(s2))!= 0){tarname=beftar(s2);
                            mv(s1,afttar(s2),1);tarname="";
                            }
                        }}
        }
  }
  }
}
