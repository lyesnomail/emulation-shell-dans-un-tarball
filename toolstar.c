int existtar(char * tar)//Fonction qui détermine si le fichier/dossier existe dans le fichier tar à condition d'etre dans le tar
{
    int fd; int n,i;
    char * bef = beftar(tar);
    char * aft = afttar(tar);
    fd = open(tarname,O_RDONLY);if (fd==-1){return -1;}
    struct posix_header *h = (struct posix_header*)malloc(sizeof(struct posix_header));
    char * p = malloc(sizeof(tar)+sizeof(char));
    memset(p,'\0',strlen(p));
    strncat(p,tar,strlen(tar));
    char ba= '/';
    if(tar[strlen(tar)-1]=='/'){ba = '\0';}
        strcat(p,&ba);
    if(dottar(tar)){
        return 1;
    }
    else{
        while (1)
          {
            n = read(fd,h,sizeof(struct posix_header ));
            if((strcmp(h->name, "\0")==0)){
                break;
            }
            if(strncmp(h->name, tar,strlen(tar)-1)==0){
                return 1;
            }
            i = 0;
            sscanf(h->size,"%o",&i);
            i = (i + BLOCKSIZE - 1) >> BLOCKBITS ;
            lseek(fd,i*BLOCKSIZE,SEEK_CUR);
            }}
    free(h);
    close(fd);
    return 0;
}
int isfile(char * pa,char * tarnm)//Fonction qui détermine si le fichier existe dans le fichier tar
{
    int fd = open(tarnm,O_RDONLY);
    struct posix_header *h = malloc(sizeof(struct posix_header));
    while (1)
    {
        int n = read(fd,h,sizeof(struct posix_header));
        if(h->name[0]=='\0' || n!= BLOCKSIZE || n==0){
            break;
        }
        if(strcmp(h->name,pa)==0){
            if (h->typeflag!='5'){
                free(h);
                close(fd);
                return 1;
            }
        }
        int i = 0;
        sscanf(h->size,"%o",&i);
        i = (i + BLOCKSIZE - 1) >> BLOCKBITS ;
        lseek(fd,i*BLOCKSIZE,SEEK_CUR);
    }
    free(h);
    close(fd);
    return 0;
}
int isdir(char *p,char * tarnm )//Fonction qui détermine si le dossier existe dans le fichier tar
{
    int fd = open(tarnm,O_RDONLY);
    char *pa = malloc(sizeof(char)*(strlen(p)+1));
    strcpy(pa,p);
    if(pa[strlen(pa)-1]!='/'){
        strcat(pa,"/");}
    struct posix_header *h = malloc(sizeof(struct posix_header));
    while (1)
    {
        int n = read(fd,h,sizeof(struct posix_header));
        if(h->name[0]=='\0' || n!= BLOCKSIZE || n==0){
            break;
        }
        if(strcmp(h->name,pa)==0){
            if (h->typeflag=='5'){
                free(h);
                close(fd);
                free(pa);
            return 1;
            }
        }
        int i = 0;
        sscanf(h->size,"%o",&i);
        i = (i + BLOCKSIZE - 1) >> BLOCKBITS ;
        lseek(fd,i*BLOCKSIZE,SEEK_CUR);
    }
    free(h);
    close(fd);
    free(pa);
    return 0;
}
int isempty(char *p, char * tarnm)//Fonction qui détermine si le dossier est vide dans le tar
{
    int fd = open(tarnm,O_RDONLY);
    char *pa = malloc(sizeof(char)*(strlen(p)+1));
    strcpy(pa,p);
    strcat(pa,"/");
    int temp = 1;
    int j;
    struct posix_header *h = malloc(sizeof(struct posix_header));
    while (1)
    {
        int n = read(fd,h,sizeof(struct posix_header));
        if(h->name[0]=='\0' || n!= BLOCKSIZE || n==0){
            break;
        }
        if (strncmp(h->name,pa,strlen(pa))==0 && strcmp(h->name,pa)!=0  )
        {
            temp = 0;
            break ;
        }
        int i;
        sscanf(h->size,"%o",&i);
        i = (i + BLOCKSIZE - 1) >> BLOCKBITS ;
        lseek(fd,i*BLOCKSIZE,SEEK_CUR);
    }
    free(h);
    close(fd);
    free(pa);
    return temp;
}
