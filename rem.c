int dir_position(char *p,char* del)//Fonction qui détermine la position du fichier à supprimer
{
   int fd = open(del,O_RDONLY);
   char *pa = malloc(sizeof(char)*(strlen(p)+1));
   strcpy(pa,p);
   strcat(pa,"/");
   struct posix_header *h = malloc(sizeof(struct posix_header));
   while (1)
   {
       int n = read(fd,h,sizeof(struct posix_header));
       if(h->name[0]=='\0' || n!= BLOCKSIZE || n==0){
           break;
       }
       if(strcmp(h->name,pa)==0){
       if (h->typeflag=='5'){
           free(h);
           int p =lseek(fd,0,SEEK_CUR);
           p=p-512;
           close(fd);
           free(pa);
           return p;
       }
   }
       int i = 0;
       sscanf(h->size,"%o",&i);
       i = (i + BLOCKSIZE - 1) >> BLOCKBITS ;
       lseek(fd,i*BLOCKSIZE,SEEK_CUR);
   }
   free(h);
   close(fd);
   free(pa);
   return 0;
}
int position_file_before(char  *pa, char * tar)//Fonction qui détermine la position qui se trouve avant le fichier à supprimer
{
    int fd = open(tar,O_RDONLY);
    struct posix_header *h = malloc(sizeof(struct posix_header));
    while (1)
    {
        int n = read(fd,h,sizeof(struct posix_header));
        if(h->name[0]=='\0' || n!= BLOCKSIZE || n==0){
            break;
        }
        if(strcmp(h->name,pa)==0){
          if (h->typeflag!='5'){
              lseek(fd,-BLOCKSIZE,SEEK_CUR);
              int p = lseek(fd,0,SEEK_CUR);
              free(h);
              close(fd);
              return p;
          }
        }
        int i = 0;
        sscanf(h->size,"%o",&i);
        i = (i + BLOCKSIZE - 1) >> BLOCKBITS ;
        lseek(fd,i*BLOCKSIZE,SEEK_CUR);
    }
    free(h);
    close(fd);
    return 0;
}
int position_file_after(char *pa,char * tar)////Fonction qui détermine la position qui se trouve aprés le fichier à supprimer
{
    int fd = open(tar,O_RDONLY);
    struct posix_header *h = malloc(sizeof(struct posix_header));
    while (1)
    {
        int n = read(fd,h,sizeof(struct posix_header));
        if(h->name[0]=='\0' || n!= BLOCKSIZE || n==0){
            break;
        }
        if(strcmp(h->name,pa)==0){
        if (h->typeflag!='5'){
            int i;
            sscanf(h->size,"%o",&i);
            i = (i + BLOCKSIZE - 1) >> BLOCKBITS ;
            lseek(fd,i*BLOCKSIZE,SEEK_CUR);
            int p = lseek(fd,0,SEEK_CUR);
            return p;
        }
    }
        int i = 0;
        sscanf(h->size,"%o",&i);
        i = (i + BLOCKSIZE - 1) >> BLOCKBITS ;
        lseek(fd,i*BLOCKSIZE,SEEK_CUR);
    }
    free(h);
    close(fd);
    return 0;
}
void remove_dir_in_tar(char *pa)//Fonction rmdir
{ 
  int pos = whenbackn(pa);char * p = malloc(sizeof(pa));
  memset(p,'\0',sizeof(p));
  if (pos!=-1){strncpy(p,pa,pos);}else {p=pa;}
  int fd;
  char * bef = beftar(p);
  char * aft = afttar(p);
  char * del;
  int z=0;
  if(tarmode==0){
    z=1;
    if(istar(p)&&!dottar(p)){
      fd = open(bef,O_RDONLY);
      del = bef;
    }
    else{
      fd = open(p,O_RDONLY);del = p;
      }}
  else{
    fd = open(tarname,O_RDONLY);del = tarname;
    if(strcmp(path,"\0")!=0){
        char * pathcopy = malloc((strlen(path)*sizeof(char)+(strlen(p)*sizeof(p))));
        memset(pathcopy,'\0',strlen(pathcopy)*sizeof(char));
        strncpy(pathcopy,path,strlen(path));
        strcat(pathcopy,"/");
        strcat(pathcopy,p);
        aft  = pathcopy;
      }
      else{ aft=p;}
    }
    if((!isdir(aft,del))||(fd<0)){
      char * msg = malloc(TAILLE*sizeof(char));memset(msg,'\0',sizeof(msg));
      char * m1 = "rmdir: ";
      char * m2 = ": Aucun fichier ou dossier de ce type";
      strcat(msg,m1);
      strcat(msg,p);
      strcat(msg,m2);
      write(STDIN_FILENO,msg,strlen(msg));
    }
    else{
       if(!isempty(aft,del)){
         char * msg = malloc(TAILLE*sizeof(char));memset(msg,'\0',sizeof(msg));
         char * m1 = "rmdir: '";
         char * m2 = "': Le dossier n'est pas vide";
         strcat(msg,m1);
         strcat(msg,aft);
         strcat(msg,m2);
         write(STDIN_FILENO,msg,strlen(msg));
       }
       else{
            int b = dir_position(aft,del);
            int new_file=open("temp.tar",O_WRONLY | O_CREAT,0777);
            char c ;
            int n ;
            while ((n = lseek(fd,0,SEEK_CUR) )< b)
            {
                read(fd,&c,1);
                write(new_file,&c,1);
            }
            lseek(fd,512,SEEK_CUR);
            while (read(fd,&c,1)!=0)
            {
                write(new_file,&c,1);
            }
            close(new_file);
            remove(del);
            rename("temp.tar",del);}}
            close(fd);

}
void remove_file(char * paa)//Fontion rm classique
{
        int pos = whenbackn(paa);char * pa = malloc(sizeof(paa));
        memset(pa,'\0',sizeof(pa));
        if (pos!=-1){strncpy(pa,paa,pos);}else {pa=paa;}
        int fd;
        char * bef = beftar(pa);
        char * aft = afttar(pa);
        char * del;
        if(tarmode==0){
            if(istar(pa)&&!dottar(pa)){
              fd = open(bef,O_RDONLY);del=bef;
            }
            else{
              fd = open(pa,O_RDONLY);}del=pa; }
        else{
          fd = open(tarname,O_RDONLY);del = tarname;
          if(strcmp(path,"\0")!=0){
            char * pathcopy = malloc((strlen(path)*sizeof(char)+(strlen(pa)*sizeof(pa))));
            memset(pathcopy,'\0',strlen(pathcopy)*sizeof(char));
            strncpy(pathcopy,path,strlen(path));
            strcat(pathcopy,"/");
            strcat(pathcopy,pa);
            aft  = pathcopy;
            }
            else{ aft=pa;}
        }
        if(((!(isfile(aft,beftar(del))||isdir(aft,beftar(del))))&&(strlen(aft)!=0))||(fd<0)){
          char * msg = malloc(TAILLE*sizeof(char));memset(msg,'\0',sizeof(msg));
          char * m1 = "rm: ";
          char * m2 = ": Aucun fichier ou dossier de ce type";
          strcat(msg,m1);
          strcat(msg,pa);
          strcat(msg,m2);
          write(STDIN_FILENO,msg,strlen(msg));
        }
        else{
            int b = position_file_before(aft,beftar(del));
            int a = position_file_after(aft,beftar(del));
            int fd1 = open ("temp",O_CREAT|O_WRONLY,0777);
            char c;
            for (size_t i = 0; i < b; i++)
            {
                read(fd,&c,1);
                write(fd1,&c,1);
            }
            lseek(fd,a,SEEK_SET);
            while (read(fd,&c,1)!=0)
            {
                write(fd1,&c,1);
            }
        remove(beftar(del));
        rename("temp",beftar(del));}
}
