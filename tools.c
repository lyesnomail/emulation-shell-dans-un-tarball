void cut(char * buf)// inspiration pour le separateur : http://www.comp.ita.br/~cecilia/ces-33/Lab1-ShellUnix.c (seul cette partie a Ã©tait reprise du code, le reste non )
{int ind = 0;
   sep[ind++] = strtok (buf," ");
   while (sep[ind-1] != NULL)
   {
   sep[ind++] = strtok (NULL, " ");}
}
int isexit(char * buf)//Fonction qui détermine si la chaine contient le mot exit
{
    char * bufcopy = malloc(strlen(buf));
    strncpy(bufcopy, buf, strlen(buf));
    char * token = strtok(bufcopy, " ");
    while( token != NULL ) {
        if ( strcmp(token,"exit") == 0) {return 1;}
        token = strtok(NULL, " ");
    }
    return 0;
}
int dottar(char* buf)// Fonction qui détermine si buf se termine par la chaine ". tar"
{
    char * dottar  = malloc(4*sizeof(char));
    char * bufcopy = malloc(strlen(buf));
    strncpy(bufcopy,buf, strlen(buf));
    strncpy(dottar,bufcopy+strlen(bufcopy)-4, 4);
    if (strcmp(dottar,".tar")==0){return 1;}
    return 0;
}
int nbrword(char * buf)//Fonction qui détermine le nombre de sous mots dans une chaine séparé par des espaces
{
    int nbr=0;
    char * bufcopy = malloc(strlen(buf));
    memset(bufcopy,'\0',strlen(bufcopy));
    strncpy(bufcopy,buf,strlen(buf));
    char * token = strtok(bufcopy," ");
    while (token != NULL)
    { nbr++;
      token = strtok(NULL," ");}
      return nbr;
}
char ** words(char * buf)//Fonction qui retourne un tableau de mots à partir d'une chaine constitué d'autres sous mots séparés par des espaces
{
    char * bufcopy = malloc(strlen(buf));
    char ** t = malloc((nbrword(buf)+1)*sizeof(char));
    memset(bufcopy,'\0',strlen(bufcopy)*sizeof(char));
    strncpy(bufcopy,buf,strlen(buf)*sizeof(char));
    char * token = strtok(bufcopy," ");
    int i = 0;
    while (token != NULL)
    {
       t[i]=malloc(sizeof(token));
       memset(t[i],'\0',strlen(t[i]));
       strncpy(t[i],token,strlen(token)*sizeof(char));
       token = strtok(NULL," ");i = i+1;}
    t[nbrword(buf)]="\0";
    free(bufcopy);free(token);
    return t;
}
int istar(char* buf)//Fonction qui détermine si une chaine contion la sous-chaine ".tar"
{
    char * bufcopy = malloc(strlen(buf));
    memset(bufcopy,'\0',sizeof(bufcopy));
    strncpy(bufcopy,buf, strlen(buf));
    char * token = strtok(bufcopy, "/");
    while( token != NULL ) {
        if (dottar(token)) {return 1;}
        token = strtok(NULL, "/");
    }
    return 0;
}
int back(char *t)//Fonction qui détermine la position du dernier "/" dans une chaine
{
      if (!t)
      {return 0;}
      int a ;int b=0;
      for (size_t i =strlen(t)-1; i!=0; i--)
      {
          if (t[i]=='/'){
              a=i;b=1;
              break;
          }
      }
      if(b==0){return 0;}
      return a;
  }
char * beftar(char * buf)//Fonction qui retourne à partir d'un chemin adonné, le chemin menant vers le fichier tar
{
  char * bef= malloc(strlen(buf));
  memset(bef,'\0',strlen(bef));
  char * bufcopy = malloc(strlen(buf));
  memset(bufcopy,'\0',strlen(bef));
  strncpy(bufcopy,buf, strlen(buf));
  char * token = strtok(bufcopy, "/");
  int ci=0;
  char * cii;char * h1;
  while( token != NULL ) {
      h1 = malloc(sizeof(token) + sizeof(char));// taille token + taille "/"
      memset(h1,'\0',sizeof(h1));
      if(ci==0){cii="";}else{cii="/";}
      strcat(h1,cii);
      strcat(h1,token);
      strcat(bef,h1 );
      if(dottar(token)==1){break;}
      ci++;
      token = strtok(NULL, "/");
      free(h1);
  }
  return bef;
}
char * afttar(char * buf)//Fonction qui retourne à partir d'un chemin adonné, le chemin qui suit après le fichier tar
{
  char * aft= malloc(strlen(buf)+1);
  memset(aft,'\0',sizeof(aft));
  char * bufcopy = malloc(strlen(buf));
  memset(bufcopy,'\0',sizeof(aft));
  strncpy(bufcopy,buf, strlen(buf));
  char * token = strtok(bufcopy, "/");
  int done=0;
  int ci=0;
  char * cii;char * h1;
  while( token != NULL ) {
      h1 = malloc(sizeof(token) + sizeof(char));// taille token + taille "/"
      memset(h1,'\0',sizeof(h1));
      if(done){
        if(ci==0){cii="";}else{cii="/";}
        strcat(h1,cii);
        strcat(h1,token);
        strcat(aft,h1 );ci++;}
      else if(dottar(token)==1){done=1;}
      token = strtok(NULL, "/");
      free(h1);
  }
  return aft;
}
char * cd(char * buf)//Fonction qui permet de faire fonctionner la commande cd hors tar
{
    char * bufcopy = malloc(strlen(buf));
    strncpy(bufcopy,buf, strlen(buf));
    char * token = strtok(bufcopy, " ");int cd=0; int i=0;
    while( token != NULL ) {
        if (( strcmp(token,"cd") == 0)) {if (cd == 0 ) {cd = 1;}}
        else if (cd) {
          if(strcmp(token,"..")!=0){return(token);}
          else {
           char *t1 = malloc(256*sizeof(char));
           getcwd(t1,256);
           int a = back(t1);
           char *n = malloc(a);
           strncpy(n,t1,a); i++;
           return n;
         }}
        token = strtok(NULL, " ");
    }
    return "";
}
int outside(char * s1, char * s2)//Fonction de traitement de chaine afin d'afficher un résultat plus proche de la vraie commande ls pour l'affichage du nom des fiches/doss
{
  char * s3 = malloc((strlen(s1)-strlen(s2))*sizeof(char));
  memset(s3,'\0',sizeof(s3));
  strncpy(s3,s1+(strlen(s2)*sizeof(char)),(strlen(s1)-1)-strlen(s2));
  int done=0;
  for (int i =0;i<(strlen(s3));i++){
    if((s3[i]!=' ')&&(done)){
      return 0;
    }
    if((s3[i]=='/')&&(i!=0)){
    done=1;
    }
  }
  return 1;
}
char * hname(char * s1, char * s2)//Même chose que précedament
{
  char * s3 = malloc((strlen(s1)-strlen(s2))*sizeof(char));
  memset(s3,'\0',sizeof(s3));
  strncpy(s3,s1+(strlen(s2)*sizeof(char)),(strlen(s1)-1)-strlen(s2));
  if (s3[0]=='/'){
    s3[0]=' ';
  }
  if (s3[strlen(s3)-1]=='/'){
    s3[strlen(s3)-1]=' ';
  }
  strcat(s3," ");
  return s3;}
int whenbackn(char * s)// Fonction qui détermine la position du caractère "/n"
{
  for(int i=0;i<strlen(s);i++){
    if(s[i]=='\n'){
      return i;
    }
  }
  return -1;
}
