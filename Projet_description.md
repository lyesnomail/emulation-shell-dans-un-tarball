SYSTÈME
==================

**L3 Informatique**


## Sujet : `tsh`, un shell pour les tarballs

Le but du projet est de faire tourner un shell qui permet à
l'utilisateur de traiter les tarballs comme s'il s'agissait de
répertoires, **sans que les tarballs ne soient désarchivés**. Le
format précis d'un tarball est décrit sur
[https://fr.wikipedia.org/wiki/Tar_(informatique)](https://fr.wikipedia.org/wiki/Tar_%28informatique%29).

Le shell demandé doit avoir les fonctionnalités suivantes :

* les commandes `cd` et `exit` doivent exister (avec leur comportement habituel)
* toutes les commandes externes doivent fonctionner normalement si leur déroulement n'implique pas l'utilisation d'un fichier (au sens large) dans un tarball
* `pwd` doit fonctionner y compris si le répertoire courant passe dans un tarball
* `mkdir`, `rmdir` et `mv` doivent fonctionner y compris avec des chemins impliquant des tarball quand ils sont utilisés sans option
* `cp` et `rm` doivent fonctionner y compris avec des chemins impliquant des tarball quand ils sont utilisés sans option ou avec l'option `-r`
* `ls` doit fonctionner y compris avec des chemins impliquant des tarball quand il est utilisé sans option ou avec l'option `-l`
* `cat` doit fonctionner y compris avec des chemins impliquant des tarball quand il est utilisé sans option
* les redirections de l'entrée, de la sortie et de la sortie erreur (y
  compris sur des fichiers d'un tarball) doivent fonctionner
* les combinaisons de commandes avec `|` doivent fonctionner

On ne vous demande pas de gérer les cas de tarballs imbriqués (un tarball dans un autre tarball).

Tous les processus lancés à partir de votre shell le seront en premier-plan.

**Les seules interdictions strictes sont les suivantes :** utilisation de
la fonction `system` de la `stdlib` et de la commande `tar` (vous
pouvez utiliser la commande `tar` pour créer des archives afin de
tester le projet, mais pas dans le code du projet). Les
entrées/sorties doivent se faire en bas niveau, sauf pour les messages
d'erreur pour lesquels `perror` est autorisé.



> 50% max (N2, 50%N1 + 50%N2) + 50% N3.
